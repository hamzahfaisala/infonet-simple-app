export default function ({ app, route, redirect }) {
  if (route.path !== "auth/signin") {
    if (!$fire.atuh.currentUser) {
      return redirect("auth/signin");
    }
  } else if (route.path === "auth/signin") {
    if (!$fire.atuh.currentUser) {
    } else {
      return redirect("/");
    }
  }
}
