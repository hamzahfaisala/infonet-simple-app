import Vue from "vue";
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "Ka5JcBchkVsJSr3gxVgHPC1NcvPO90lG",
    libraries: "places",
  },
});
